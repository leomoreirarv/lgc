const presets = [
  "@babel/env",
  "@babel/typescript"
];

const plugins = [
  "@babel/proposal-class-properties",
  "@babel/proposal-object-rest-spread"
];
const env = {
}

module.exports = {
  presets,
  plugins,
  env
};