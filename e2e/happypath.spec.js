
const getClassList = (elem) => {
  return elem.getAttribute('class').split(' ');
};

describe('LetsGetChecked Blog', () => {
  describe('Main Page', () => {
    it('should have the right title', () => {
      browser.url('http://localhost:9000/');
      const title = browser.getTitle();
      expect(browser).toHaveTitle('LetsGetChecked');
    });

    describe('Sort List Buttons', () => {
      it('should have button sort post list by date active and interact properly', () => {
        const btnSortByDate = $('#sortByDate');
        btnSortByDate.waitForDisplayed({ timeout: 3000 });
        expect(getClassList(btnSortByDate)).toContain('lgc-sort--btn--active');
        expect(getClassList(btnSortByDate)).toContain('lgc-sort--btn--active--down');
        btnSortByDate.click();
        expect(getClassList(btnSortByDate)).toContain('lgc-sort--btn--active--up');
        btnSortByDate.click();
        expect(getClassList(btnSortByDate)).toContain('lgc-sort--btn--active--down');
      });


      it('should have button sort post list by date active and interact properly', () => {
        const btnSortByAuthor = $('#sortByAuthor');
        btnSortByAuthor.waitForDisplayed({ timeout: 3000 });
        btnSortByAuthor.click();
        expect(getClassList(btnSortByAuthor)).toContain('lgc-sort--btn--active');
        expect(getClassList(btnSortByAuthor)).toContain('lgc-sort--btn--active--up');
        btnSortByAuthor.click();
        expect(getClassList(btnSortByAuthor)).toContain('lgc-sort--btn--active--down');
        btnSortByAuthor.click();
        expect(getClassList(btnSortByAuthor)).toContain('lgc-sort--btn--active--up');
      });

      it('should have button sort post list by date active and interact properly', () => {
        const btnSortByTitle = $('#sortByTitle');
        btnSortByTitle.waitForDisplayed({ timeout: 3000 });
        btnSortByTitle.click();
        expect(getClassList(btnSortByTitle)).toContain('lgc-sort--btn--active');
        expect(getClassList(btnSortByTitle)).toContain('lgc-sort--btn--active--down');
        btnSortByTitle.click();
        expect(getClassList(btnSortByTitle)).toContain('lgc-sort--btn--active--up');
        btnSortByTitle.click();
        expect(getClassList(btnSortByTitle)).toContain('lgc-sort--btn--active--down');
      });
    });

    describe('Post Item interaction', () => {
      it('should have a clickable post link', () => {
        const postList = $('#postsList').$$('.card')[0];
        const firstLink = postList.$('a.card-link');
        expect(firstLink).toHaveHrefContaining('#/post/');
        firstLink.click();
      });
    });

    describe('Post Page', () => {
      it('should have both fields invalid', () => {
        const form = $('#formComment');
        const userInputField = form.$('#user');
        const commentInputField = form.$('#comment');
        const btnAddComment = form.$('#btnAddComment');
        form.waitForDisplayed({ timeout: 3000 });
        btnAddComment.click();
        expect(getClassList(userInputField)).toContain('is-invalid');
        expect(getClassList(commentInputField)).toContain('is-invalid');

        it('should comment field be invalid', () => {
          userInputField.setValue('Some name');
          btnAddComment.click();
          expect(getClassList(userInputField)).toContain('is-valid');
          expect(getClassList(commentInputField)).toContain('is-invalid');
        });

        it('should user field be invalid', () => {
          userInputField.setValue('');
          commentInputField.setValue('Some coment');
          btnAddComment.click();
          expect(getClassList(userInputField)).toContain('is-invalid');
          expect(getClassList(commentInputField)).toContain('is-valid');
        });

        it('should submit the form', () => {
          userInputField.setValue('Some name');
          commentInputField.setValue('Some coment');
          btnAddComment.click();
        });

        it('should a valid comment', () => {
          const commentList = $('#postCommentsList');
          commentList.waitForDisplayed({ timeout: 3000 });

          const firstComment = commentList.$$('.card')[0];
          expect(firstComment.$('.card-header')).toHaveTextContaining('Some name');
          expect(firstComment.$('.card-text')).toHaveTextContaining('Some coment');

          const logoLink = $('.navbar-brand');
          logoLink.click();
        });
      });
    });
  });
});