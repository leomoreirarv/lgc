const path = require('path');
const webpack = require('webpack');
const htmlWebpackPlugin = require('html-webpack-plugin');
const TerserPlugin = require('terser-webpack-plugin');
const RobotstxtPlugin = require("robotstxt-webpack-plugin");
const FaviconsWebpackPlugin = require('favicons-webpack-plugin')

const SOURCE = path.resolve(__dirname, 'src');
const PROD = path.resolve(__dirname, 'dist');

module.exports = {
  context: SOURCE,
  mode: 'development',
  entry: {
    'bootstrap-sass': 'bootstrap/scss/bootstrap',
    'bootstrap': 'bootstrap',
    'main-page': './views/pages/main/MainPage.class.ts',
    'post-page': './views/pages/post/PostPage.class.ts',
    'comment-form-view': './views/components/CommentForm/CommentFormView.class.ts',
    'main-view': './views/components/Main/MainView.class.ts',
    'post-comment-view': './views/components/PostComment/PostCommentView.class.ts',
    'post-detail-view': './views/components/PostDetail/PostDetailView.class.ts',
    'post-list-view': './views/components/PostList/PostListView.class.ts',
    'sort-view': './views/components/Sort/SortView.class.ts',
    'router': './helper/router/Router.class.ts',
    'index': './index.ts'
  },
  output: {
    filename: '[name].bundle.js',
    path: PROD
  },
  optimization: {
    minimize: true,
    minimizer: [new TerserPlugin({
      sourceMap: true,
      parallel: true,
      terserOptions: {
        ecma: 6,
      },
    })],
  },
  watch: true,
  resolve: {
    extensions: ['.ts', '.js', '.html', '.scss'],
    modules: [
      SOURCE,
      'node_modules'
    ]
  },
  module: {
    rules: [{
        enforce: 'pre',
        test: /\.js$/,
        use: 'source-map-loader'
      },
      {
        enforce: 'pre',
        test: /\.ts$/,
        exclude: /node_modules/,
        use: 'tslint-loader'
      },

      {
        test: /\.ts$/,
        exclude: [/node_modules/],
        use: 'awesome-typescript-loader'
      },
      {
        test: /.html$/,
        use: 'raw-loader'
      },
      {
        test: /\.(woff|woff2|ttf|svg|eot)$/,
        loader: 'url-loader'
      },
      {
        test: /\.s?[ac]ss$/,
        use: [{
            loader: 'style-loader', 
          }, {
            loader: 'css-loader',
            options: {
              sourceMap: true
            }
          }, {
            loader: 'postcss-loader',
            options: {
              plugins: function () {
                return [
                  require('precss'),
                  require('autoprefixer')
                ];
              }
            }
          },
          {
            loader: 'resolve-url-loader'
          },
          {
            loader: 'sass-loader' // compiles Sass to CSS
          }
        ]
      }
    ]
  },
  devtool: 'eval-source-map',
  externals: {
    bootstrap: {
      jQuery: 'jquery',
      $: 'jquery',
      Util: 'Util',
      Alert: 'Alert',
      Button: 'Button',
      Carousel: 'Carousel',
      Collapse: 'Collapse',
      Dropdown: 'Dropdown',
      Modal: 'Modal',
      Popover: 'Popover',
      Scrollspy: 'Scrollspy',
      Tab: 'Tab',
      Tooltip: 'Tooltip'
    }
  },
  devServer: {
    contentBase: SOURCE,
    watchContentBase: true
  },
  plugins: [
    new htmlWebpackPlugin({
      template: './index.html'
    }),
    new RobotstxtPlugin({}),
    new FaviconsWebpackPlugin('../favico.png')
  ]
};