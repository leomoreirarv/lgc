module.exports = {
  process(content) {
    return `module.exports = { default: '${JSON.stringify(content)}' }`;
  },
  getCacheKey() {
    return 'htmlTransform';
  },
};