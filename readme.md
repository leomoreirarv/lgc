# LetsGetChecked Blog

## About

LetsGetChecked Blog is a code assestment made in Typescript. 
The blog was implementated as a SPA (Single Page Application), WCGA and SEO friendly.
Highlight points:

- **Typescript/Javascript** - The application was developted 100% in typescript, whithout any other dependency;
- **WCGA friendly** - Following Web Content Accessibility Guidelines the application is suitable for users with special needs;
- **SPA** - A single HTML document is loaded when the blog is launched. DOM manipulation is performaded by javascript;
- **URL SEO Friendly** - A own routering controller has been made for that application;
- **Unity Testable** - Core application has a good test corage (by Jest);
- **E2E Automation** - View application has a automated test (by WebDriverIO);

#### Technologies
![html 5](https://assestmentlgc.s3-eu-west-1.amazonaws.com/logos/html.png) 
![javascript](https://assestmentlgc.s3-eu-west-1.amazonaws.com/logos/javascript.png) 
![typescript](https://assestmentlgc.s3-eu-west-1.amazonaws.com/logos/typescript.png) 
![sass](https://assestmentlgc.s3-eu-west-1.amazonaws.com/logos/sass.jpg) 
![babel](https://assestmentlgc.s3-eu-west-1.amazonaws.com/logos/babel.png) 
![webpack](https://assestmentlgc.s3-eu-west-1.amazonaws.com/logos/webpack.png) 
![jest](https://assestmentlgc.s3-eu-west-1.amazonaws.com/logos/jest.png) 
![webdriverio](https://assestmentlgc.s3-eu-west-1.amazonaws.com/logos/webdriverio.png)
![Amazon Webservices S3](https://assestmentlgc.s3-eu-west-1.amazonaws.com/logos/aws-s3.png)

## Getting Started

In order to run the frontend application please follow the instructions bellow. 

### Prerequisites

What things you need to install the software and how to install them.

You must have install NodeJS (latest version) [https://nodejs.org/en/](https://nodejs.org/en/)

### Installing Frontend Application

```
git clone https://leomoreirarv@bitbucket.org/leomoreirarv/lgc.git
```

```
npm i
```

### Installing Mock API
```
git clone https://github.com/LetsGetChecked/developer-challenge-api
```
```
npm i
```
#### Running API
```
npm run api
```

## Usage

Keep in mind that to use the application you need run the mock api by following the steps above.

### Running project in developmenet environment
```
npm start
```
### Running unit test
```
npm test
```

### Running test e2e
```
npm run e2e
```

### Accessing published Blog
LestGetChecked Blog [(https://assestmentlgc.s3-eu-west-1.amazonaws.com/)](https://assestmentlgc.s3-eu-west-1.amazonaws.com/index.html#) is hosted at Amazon S3, please note that **you must have the mock API running locally** in order to see the application properly.

