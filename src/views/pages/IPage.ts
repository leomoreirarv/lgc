import { IRoute } from '../../helper/router/IRoute';
import IScope from '../../IScope';

export abstract class Page implements IPage {
  scope: IScope;
  detail: IRoute;

  constructor(_scope: IScope) {
    this.scope = _scope;
  }
}

export interface IPage {
  scope: IScope;
  detail: IRoute;
}
