jest.mock('../../components/Sort/SortView.class');
jest.mock('../../components/PostList/PostListView.class');

import IScope from 'IScope';
import MainPage from './MainPage.class';

describe('Main Page', () => {
  let mainPage: MainPage;

  beforeAll(() => {
    document.body.innerHTML = '<div id="mainContent"></div>';
    mainPage = new MainPage(expect.any(<IScope>{}));
  });

  it('should have valid page ', async () => {
    await mainPage.init();
    expect(mainPage).toBeTruthy();
  });
});