import IScope from '../../../IScope';
import MainView from '../../components/Main/MainView.class';
import SortView from '../../components/Sort/SortView.class';
import { BlogEvents } from '../../../enums/BlogEvents.enum';
import PostListView from '../../components/PostList/PostListView.class';
import { Page } from '../IPage';
import { IRoute } from '../../../helper/router/IRoute';

export default class MainPage extends Page {
  scope: IScope;
  detail: IRoute;

  constructor(_scope: IScope) {
    super(_scope);
    this.scope = _scope;
  }

  async init(_detail: IRoute = null) {
    this.detail = _detail;
    const mainView = new MainView(this.scope);
    const postListView = new PostListView(this.scope.posts);
    await mainView.init();
    await postListView.init();

    const sortView = new SortView(this.scope);
    await sortView.init();
    await postListView.templateLoader(this.scope.posts);

    window.addEventListener(BlogEvents.POSTISTUPDATE, (e: CustomEvent) => {
      postListView.templateLoader(e.detail.posts);
    });
  }
}

