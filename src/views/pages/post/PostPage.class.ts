import { Page, IPage } from './../IPage';
import IScope, { IComment } from '../../../IScope';
import PostDetailView from '../../components/PostDetail/PostDetailView.class';
import { IRoute } from '../../../helper/router/IRoute';
import PostService from '../../../services/Post/PostService.class';
import { IPost } from '../../components/PostList/IPost';
import PostComment from '../../components/PostComment/PostCommentView.class';
import CommentForm from '../../components/CommentForm/CommentFormView.class';
import { BlogEvents } from '../../../enums/BlogEvents.enum';

export default class PostPage extends Page {
  scope: IScope;
  detail: IRoute;

  constructor(_scope: IScope) {
    super(_scope);
    this.scope = _scope;
  }

  async init(_detail: IRoute = null) {

    this.detail = _detail;

    if (this.detail) {
      await this._loadPost(this.detail.data.id).then(async selected => {
        this.scope = Object.assign(this.scope, { selected });
      });

      await this._loadComments(this.detail.data.id).then(selectedComments => {
        this._nestedCommentdsBuilder(selectedComments);
      });

      const postDetailView = new PostDetailView(this.scope);
      const postComment = new PostComment(this.scope.selectedComments);
      const commentForm = new CommentForm('#postCommentForm', null, this.scope.selected.id);

      await postDetailView.init();
      await commentForm.init();
      await postComment.init();

      this.postCommentsUpdateListener(postComment);
    }

  }

  private postCommentsUpdateListener(postComment: PostComment) {
    window.addEventListener(BlogEvents.ONCOMMENTSUPDATE, async () => {
      await this._loadComments(this.detail.data.id).then(selectedComments => {
        this._nestedCommentdsBuilder(selectedComments);
        postComment = new PostComment(this.scope.selectedComments);
        postComment.init();
      });
    });
  }

  private _nestedCommentdsBuilder(selectedComments: IComment[]) {
    selectedComments.sort((a, b) => a.date < b.date ? 1 : a.date > b.date ? -1 : 0);
    let nestedComments: IComment[] = [];
    selectedComments.forEach(comment => {
      if (comment.parent_id === null) {
        nestedComments.push(comment);
      }
    });
    selectedComments.forEach(comment => {
      if (comment.parent_id !== null) {
        const parent = nestedComments.filter(c => c.id === comment.parent_id)[0];
        if (parent) {
          parent.nested.push(comment);
        }
      }
    });
    this.scope = Object.assign(this.scope, { selectedComments: nestedComments });
  }

  private async _loadPost(id: number): Promise<IPost> {
    return await PostService.loadAPost(id).then(response => response);
  }

  private async _loadComments(id: number): Promise<IComment[]> {
    return await PostService.loadAPostComments(id).then(response => response.map(res => {
      return Object.assign(res, { nested: [] });
    }));
  }
}