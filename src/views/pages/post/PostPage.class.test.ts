jest.mock('../../components/CommentForm/CommentFormView.class');
jest.mock('../../components/PostComment/PostCommentView.class');
jest.mock('../../components/PostDetail/PostDetailView.class');

import PostPage from './PostPage.class';
import IScope from 'IScope';

describe('Post Page', () => {
    let postPage: PostPage;

    beforeAll(() => {
        postPage = new PostPage(expect.any(<IScope>{}));
    });

    it('should have valid page ', async () => {
        await postPage.init();
        expect(postPage).toBeTruthy();
    });
});