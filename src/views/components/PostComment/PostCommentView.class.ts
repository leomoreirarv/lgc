import ViewBuilder from '../ViewBuilder/ViewBuilder.class';

const template = require('./postcommentview.template.html').default;

export default class PostComment extends ViewBuilder {
  constructor(scope: Object, target: string = '#postCommentsList', isList: boolean = true) {
    super(template, target, scope, isList);
  }

  async init() {
    await super.init();
    const isScopeAList = !this.scope.hasOwnProperty('id');

    if (isScopeAList) {
      Object.keys(this.scope).map(async key => {
        const scopeItem: any = (this.scope as any)[key];

        const scopeNest = scopeItem.nested;

        if (scopeNest.length > 0) {
          const nestedPostComment = new PostComment(scopeNest, `#commentsReplies-${scopeItem.id}`, true);
          await nestedPostComment.init();
        }
      });
    }
  }
}