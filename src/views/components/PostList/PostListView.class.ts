import ViewBuilder from '../ViewBuilder/ViewBuilder.class';
import { BlogEvents } from '../../../enums/BlogEvents.enum';
import { Routes } from '../../../enums/Routes.emum';
import { IRoute } from '../../../helper/router/IRoute';

const template = require('.//postlistview.template.html').default;

export default class PostListView extends ViewBuilder {
  constructor(scope: Object = {}) {
    super(template, '#postsList', scope, true);
  }

  async init() {
    await super.init();
    this.addListener();
  }

  addListener() {
    document.querySelector('#postsList').addEventListener('click', (event: MouseEvent) => {
      const ele = <HTMLElement>event.target;
      if (ele.classList.contains('card-link')) {
        const id = parseInt(ele.getAttribute('blog-data-id'));
        const slug = ele.getAttribute('blog-data-slug');
        const eventData: CustomEventInit<IRoute> = {
          detail: {
            route: Routes.POST,
            path: `#/post/${slug}`,
            data: {
              id,
              slug
            },
            page: null,
            scope: this.scope,
            isDefault: false
          }
        };

        window.dispatchEvent(new CustomEvent<IRoute>(BlogEvents.ONNAVIGATE, eventData));
      }
    });
  }
}