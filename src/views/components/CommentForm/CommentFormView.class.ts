import ViewBuilder from '../ViewBuilder/ViewBuilder.class';
import { ICommentForm } from './ICommentForm';
import PostService from '../../../services/Post/PostService.class';
import { BlogEvents } from '../../../enums/BlogEvents.enum';

const template = require('./commentformview.template.html').default;

export default class CommentForm extends ViewBuilder {
  parentId: number;
  postId: number;

  constructor(target: string, _parentId: number = null, _postId: number) {
    super(template, target);
    this.parentId = _parentId;
    this.postId = _postId;
  }

  async init() {
    await super.init();
    const formComment = document.querySelector(`${this.target} #formComment`);
    this.addSubmitFormListener(formComment);
  }

  private addSubmitFormListener(formComment: Element) {
    formComment.addEventListener('submit', this.formSubmitHandler.bind(this));
  }

  private formCleanup(inputUser: HTMLInputElement, inputComment: HTMLTextAreaElement) {
    inputUser.classList.remove('is-valid');
    inputUser.value = '';
    inputComment.classList.remove('is-valid');
    inputComment.value = '';
  }

  private readonly formSubmitHandler = async (event: Event): Promise<void> => {
    event.preventDefault();
    event.stopPropagation();
    const form = <HTMLFormElement>event.target;
    const inputUser = <HTMLInputElement>document.querySelector(`${this.target} #user`);
    const inputComment = <HTMLTextAreaElement>document.querySelector(`${this.target} #comment`);
    const formValidity = this.checkValidityForm(form, inputUser, inputComment);
    if (formValidity) {
      const commentData: ICommentForm = {
        parent_id: this.parentId,
        user: inputUser.value,
        date: (new Date()).toISOString().split('T')[0],
        content: inputComment.value
      };
      await PostService.addCommentAtPost(this.postId, commentData).then(data => {
        const eventData: CustomEventInit<any> = data;
        window.dispatchEvent(new CustomEvent<any>(BlogEvents.ONCOMMENTSUPDATE, eventData));
        this.formCleanup(inputUser, inputComment);
      });
    }
  }

  private checkValidityForm(form: HTMLFormElement, inputUser: HTMLInputElement, inputComment: HTMLTextAreaElement) {
    const formValidity = form.checkValidity();
    if (!formValidity) {
      this.checkValidityField(inputUser);
      this.checkValidityField(inputComment);
    }
    else {
      inputComment.classList.add('is-valid');
      inputComment.classList.remove('is-invalid');
      inputUser.classList.add('is-valid');
      inputUser.classList.remove('is-invalid');
    }

    return formValidity;
  }

  private checkValidityField(field: HTMLInputElement | HTMLTextAreaElement) {
    if (field.checkValidity()) {
      field.classList.add('is-valid');
      field.classList.remove('is-invalid');
    } else {
      field.classList.add('is-invalid');
      field.classList.remove('is-valid');
    }
  }
}

