export interface ICommentForm {
  parent_id: number;
  user: string;
  date: string;
  content: string;
}
