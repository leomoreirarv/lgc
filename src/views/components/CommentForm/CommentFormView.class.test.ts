import CommentForm from './CommentFormView.class';


describe('Comment Form View', () => {
  let commentForm: CommentForm;

  beforeAll(() => {
    document.body.innerHTML = '<div id="testTarget"></div>';
    commentForm = new CommentForm('#testTarget', null, 1);
  });

  it('should have a valid view', () => {
    expect(commentForm).toBeTruthy();
  });

  it('should listener form submition', async () => {
    const formSubmitListener = jest.spyOn(Element.prototype, 'addEventListener');
    await commentForm.init();
    expect(formSubmitListener).toHaveBeenCalled();
  });
});