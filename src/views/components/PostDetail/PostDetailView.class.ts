import ViewBuilder from '../ViewBuilder/ViewBuilder.class';

const template = require('./postdetailview.template.html').default;

export default class PostDetailView extends ViewBuilder {
  constructor(scope: Object) {
    super(template, '#mainContent', scope);
  }
}
