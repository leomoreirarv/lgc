import PostDetailView from './PostDetailView.class';

describe('Post Detail View', () => {
    let postDetailView: PostDetailView;

    beforeAll(() => {
      document.body.innerHTML = '<div id="mainContent"></div>';
      postDetailView = new PostDetailView({});
    });

    it('should have a valid view', () => {
        expect(postDetailView).toBeTruthy();
    });
});