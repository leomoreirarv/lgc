import './SortView.scss';
import ViewBuilder from '../ViewBuilder/ViewBuilder.class';
import { SortOrder } from '../../../enums/SortOrder.enum';
import IScope from '../../../IScope';
import { SortType } from '../../../enums/SortType.enum';
import { BlogEvents } from '../../../enums/BlogEvents.enum';

const template = require('./sortview.template.html').default;

export default class SortView extends ViewBuilder {
  scope: IScope;
  sortOrder: SortOrder = SortOrder.desc;

  constructor(_scope: IScope) {
    super(template, '#sortGroup', _scope);
    this.scope = _scope;
  }

  async init() {
    await super.init();
    this.startClickListeners();
  }


  private startClickListeners() {
    const btnSortByDate = <HTMLButtonElement>document.querySelector('#sortByDate');
    const btnSortByAuthor = <HTMLButtonElement>document.querySelector('#sortByAuthor');
    const btnSortByTitle = <HTMLButtonElement>document.querySelector('#sortByTitle');

    this.sortInit(SortType.date, btnSortByDate);

    btnSortByDate.addEventListener('click', this._btnSortClickHandler.bind(this, SortType.date));
    btnSortByAuthor.addEventListener('click', this._btnSortClickHandler.bind(this, SortType.author));
    btnSortByTitle.addEventListener('click', this._btnSortClickHandler.bind(this, SortType.title));
  }

  private async sortDesc(sortType: SortType) {
    this.scope.posts.sort((a, b) => {
      let order: number = 0;
      if (a[sortType] < b[sortType]) {
        order = 1;
      }
      else if (a[sortType] > b[sortType]) {
        order = -1;
      }
      return order;
    });
  }

  private async _sortByDateAsc(sortType: SortType) {
    this.scope.posts.sort((a, b) => {
      let order: number = 0;
      if (a[sortType] > b[sortType]) {
        order = 1;
      }
      else if (a[sortType] < b[sortType]) {
        order = -1;
      }
      return order;
    });
  }

  private _btnSortClickHandler(sortType: SortType, event: MouseEvent) {
    const targetButton = <HTMLButtonElement>event.target;
    this.sortInit(sortType, targetButton);
  }

  private sortInit(sortType: SortType, targetButton: HTMLButtonElement) {
    this.sortOrder === SortOrder.desc ? this.sortDesc(sortType) : this._sortByDateAsc(sortType);
    this.buttonsStyle(targetButton);
    window.dispatchEvent(new CustomEvent<IScope>(BlogEvents.POSTISTUPDATE, { detail: this.scope }));
    this.sortOrder === SortOrder.desc ? this.sortOrder = SortOrder.asc : this.sortOrder = SortOrder.desc;
  }

  private buttonsStyle(targetButton: HTMLButtonElement) {
    targetButton.parentElement.querySelectorAll('button').forEach(b => {
      b.classList.remove('btn-primary', 'lgc-sort--btn--active');
      b.classList.remove('lgc-sort--btn--active--up');
      b.classList.remove('lgc-sort--btn--active--down');
      b.classList.add('btn-secondary');
    });
    targetButton.classList.remove('btn-secondary');
    targetButton.classList.add('btn-primary', 'lgc-sort--btn--active');

    if (this.sortOrder === SortOrder.desc) {
      targetButton.classList.remove('lgc-sort--btn--active--up');
      targetButton.classList.add('lgc-sort--btn--active--down');
    } else {
      targetButton.classList.remove('lgc-sort--btn--active--down');
      targetButton.classList.add('lgc-sort--btn--active--up');
    }
  }
}