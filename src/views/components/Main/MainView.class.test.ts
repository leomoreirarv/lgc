import MainView from './MainView.class';

describe('Main View', () => {
    let mainView: MainView;

    beforeAll(() => {
      document.body.innerHTML = '<div id="mainContent"></div>';
      mainView = new MainView({});
    });

    it('should have a valid view', () => {
        expect(mainView).toBeTruthy();
    });
});