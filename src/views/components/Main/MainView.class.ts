import ViewBuilder from '../ViewBuilder/ViewBuilder.class';
const template = require('./mainview.template.html').default;

export default class MainView extends ViewBuilder {
  constructor(scope: Object) {
    super(template, '#mainContent', scope);
  }
}
