import TemplateService from '../../../services/Template/TemplateService.class';

export default abstract class ViewBuilder {

  scope: Object;
  template: string;
  target: string;
  isList: boolean;

  constructor(_template: string, _target: string, _scope: Object | any[] = {}, _isList: boolean = false) {
    this.scope = _scope;
    this.template = _template;
    this.target = _target;
    this.isList = _isList;
  }

  public async init() {
    await this.templateLoader();
  }

  public async templateLoader(newScope: Object = {}): Promise<string> {
    return new Promise((resolve, reject) => {
      try {
        const html = TemplateService
          .init(this.template, Object.assign(this.scope, newScope), this.isList)
          .processHTML();

        this._templateHandler(html).then(() => {
          resolve();
        });

      } catch (e) {
        reject(e);
      }
    });
  }

  private async _templateHandler(template: string): Promise<null> {
    return new Promise((resolve, reject) => {
      try {
        const target = document.querySelector(this.target);
        target.innerHTML = template;
        resolve();
      } catch (e) {
        reject(e);
      }
    });
  }
}