export enum SortType {
  date = 'publish_date',
  author = 'author',
  title = 'title'
}
