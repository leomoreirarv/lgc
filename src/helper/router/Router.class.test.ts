jest.mock('../../views/pages/main/MainPage.class');
jest.mock('../../views/pages/post/PostPage.class');

import { IRoute } from './IRoute';
import Router from './Router.class';
import { BlogEvents } from '../../enums/BlogEvents.enum';
import MainPage from '../../views/pages/main/MainPage.class';
import PostPage from '../../views/pages/post/PostPage.class';
import { set } from 'lodash';
import IScope from 'IScope';
import { Routes } from '../../enums/Routes.emum';

describe('Router Class', () => {
  let router: Router;

  beforeAll(() => {
    set(window, '__scope__.posts', [{
      id: 1,
      slug: '1'
    }]);
    router = new Router();
    router.add(defaultRouteBuilder());
    router.add(postRouteBuilder());
  });

  afterAll(() => {
    jest.clearAllMocks();
  });

  it('should add routes', () => {
    expect(router.routes.length).toBe(2);
  });

  it('should start event listeners', () => {
    const windowAddEventListenerSpy = jest.spyOn(window, 'addEventListener');
    router.init();
    expect(windowAddEventListenerSpy).toHaveBeenCalledWith(BlogEvents.ONLOAD, expect.anything());
    expect(windowAddEventListenerSpy).toHaveBeenCalledWith(BlogEvents.ONPOPSTATE, expect.anything());
    expect(windowAddEventListenerSpy).toHaveBeenCalledWith(BlogEvents.ONNAVIGATE, expect.anything());
  });

  it('should start main (default) page', () => {
    const defaultPageInitSpy = jest.spyOn(MainPage.prototype, 'init');
    window.dispatchEvent(new CustomEvent<IRoute>(BlogEvents.ONLOAD, {}));
    expect(defaultPageInitSpy).toHaveBeenCalled();
  });

  it('should start post page', () => {
    const postPageInitSpy = jest.spyOn(PostPage.prototype, 'init');
    const IRoutePostData = { detail: postRouteBuilder() };
    const postCustomEvent = new CustomEvent<IRoute>(BlogEvents.ONLOAD, IRoutePostData);
    window.dispatchEvent(postCustomEvent);
    expect(postPageInitSpy).toHaveBeenCalled();
  });
});

function defaultRouteBuilder() {
  const defaultRoute = expect.any(<IRoute>{});
  set(defaultRoute, 'route', Routes.MAIN);
  set(defaultRoute, 'path', '/#');
  set(defaultRoute, 'data', null);
  set(defaultRoute, 'page', MainPage);
  set(defaultRoute, 'scope', expect.any(<IScope>{}));
  set(defaultRoute, 'isDefault', true);

  return defaultRoute;
}

function postRouteBuilder() {
  const postRoute = expect.any(<IRoute>{});
  set(postRoute, 'route', Routes.POST);
  set(postRoute, 'path', '#/post/1');
  set(postRoute, 'data', { id: 1, slug: '1' });
  set(postRoute, 'page', PostPage);
  set(postRoute, 'scope', expect.any(<IScope>{}));
  set(postRoute, 'isDefault', false);

  return postRoute;
}
