import { Page } from '../../views/pages/IPage';
import { Routes } from '../../enums/Routes.emum';

export interface IRoute {
  route: Routes;
  path: string;
  data: IRouteData;
  page: Object;
  scope: Object;
  isDefault: Boolean;
}

export interface IRouteData {
  id: number;
  slug: string;
}
