import { IPost } from './../../views/components/PostList/IPost';
import { Routes } from '../../enums/Routes.emum';
import { IRoute, IRouteData } from './IRoute';
import { BlogEvents } from '../../enums/BlogEvents.enum';

export default class Router {
  routes: IRoute[] = [];

  constructor() {
  }

  add(route: IRoute) {
    this.routes.push(route);
  }

  init() {
    this._onLoadListener();
    this._popstateListener();
    this._navigateListener();

    this._routeTrigger(null);
  }


  private _onLoadListener() {
    window.addEventListener(BlogEvents.ONLOAD, this._routeTrigger.bind(this));
  }

  private _routeTrigger(event: Event | CustomEvent<IRoute>) {
    let hash: string;
    if (event instanceof CustomEvent && event.detail) {
      hash = event.detail.path.toString().replace(/#\//, '');
    } else {
      hash = window.location.hash.toString().replace(/#\//, '');
    }

    if (!hash) {
      this._gotoDefaultRoute();
    } else {
      this._gotoOtherRoute(hash);
    }
  }

  private _gotoOtherRoute(hash: string) {
    try {
      const route = this._getRouteFromHash(hash);
      const slug = this._getSlugFromHash(hash);
      const id = this._getIdFromHash(slug);
      if (id === null) {
        this._tearDown();
        return;
      }
      const currentRoute = this.routes.filter(r => (r.route).toString() === route)[0];
      const routeData: IRouteData = {
        id: id,
        slug
      };
      const eventData: CustomEventInit<IRoute> = this._customEventInitBuilder(routeData, `#/${route}/${slug}`, currentRoute.page, currentRoute.scope, false);
      const pageObject = new (currentRoute.page as any)(currentRoute.scope);
      pageObject.init(eventData.detail);
      history.pushState({}, null, eventData.detail.path);
    } catch (e) {
      console.error('[Router Class]', e);
    }
  }

  private _gotoDefaultRoute() {
    const defaultRoute = this.routes.filter(route => route.isDefault)[0];
    const pageObject = new (defaultRoute.page as any)(defaultRoute.scope);
    pageObject.init(null);
    window.history.pushState({}, null, defaultRoute.path);
  }

  private _getRouteFromHash(hash: string) {
    return hash.split('/')[0];
  }

  private _customEventInitBuilder(data: IRouteData, path: string, page: Object, scope: Object, isDefault: Boolean): CustomEventInit<IRoute> {
    return {
      detail: {
        route: Routes.POST,
        data,
        path,
        page,
        scope,
        isDefault
      }
    };
  }

  private _getIdFromHash(slug: string) {
    const posts: IPost[] = (window as any).__scope__.posts;
    const selectedPost: IPost = posts.filter(item => item.slug === slug)[0];
    if (selectedPost) {
      return selectedPost.id;
    } else {
      return null;
    }
  }

  private _getSlugFromHash(hash: string) {
    return hash.split('/')[1];
  }

  private _popstateListener() {
    window.addEventListener(BlogEvents.ONPOPSTATE, this._routeTrigger.bind(this));
  }

  private _navigateListener() {
    window.addEventListener(BlogEvents.ONNAVIGATE, this._routeTrigger.bind(this));
  }

  private _tearDown() {
    this._gotoDefaultRoute();
    throw new Error('Sorry, I don\'t know requested route.');
  }
}
