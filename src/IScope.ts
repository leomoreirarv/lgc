import { IPost } from './views/components/PostList/IPost';

export default interface IScope {
  posts: IPost[];
  selected: IPost;
  selectedComments: IComment[];
}

export interface IComment {
  id: number;
  postId: number;
  parent_id: number;
  user: string;
  date: Date;
  content: string;
  nested: IComment[];
}