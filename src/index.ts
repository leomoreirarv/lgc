import { IPost } from './views/components/PostList/IPost';
import 'index.scss';
import MainPage from './views/pages/main/MainPage.class';
import PostPage from './views/pages/post/PostPage.class';
import Router from './helper/router/Router.class';
import { Routes } from './enums/Routes.emum';
import IScope from './IScope';
import PostService from './services/Post/PostService.class';


(window as any).__scope__ = <IScope>{
  posts: [],
  selected: null,
  selectedComments: []
};

let scope: IScope = (window as any).__scope__;

async function firstLoad(): Promise<IScope> {
  const posts = await PostService.loadAllPosts().then(posts => posts).catch(() => []);
  return Object.assign(scope, { posts });
}

async function initApp() {
  await firstLoad();

  const router = new Router();

  router.add({
    route: Routes.MAIN,
    path: '/index.html#',
    data: null,
    page: MainPage,
    scope: scope,
    isDefault: true
  });

  router.add({
    route: Routes.POST,
    path: '/index.html#/post',
    data: null,
    page: PostPage,
    scope: scope,
    isDefault: false
  });

  router.init();
}

initApp();