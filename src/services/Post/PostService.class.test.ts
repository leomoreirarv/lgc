import PostService from './PostService.class';

describe('Post Service', () => {
  it('should have a valid service', () => {
    expect(PostService).toBeTruthy();
  });

  describe('loadAllPosts', () => {
    it('should retrieve data', (done) => {
      fetchMock.mockResponseOnce(JSON.stringify({ data: 'ok', status: 200 }));
      PostService.loadAllPosts().then(data => {
        expect(data).toEqual({ data: 'ok', status: 200 });
        done();
      });
    });

    it('should reject request when fetch catch an error', (done) => {
      fetchMock.mockRejectOnce(Error('Rejected Resquest'));
      PostService.loadAllPosts().then().catch(error => {
        expect(error).toBe('loadAllPosts:request => Error: Rejected Resquest');
        done();
      });
    });

    it('should reject request when fetch throw an error', (done) => {
      jest.spyOn(globalThis, 'fetch').mockImplementationOnce(() => {
        throw new Error('Execution Error');
      });
      PostService.loadAllPosts().then().catch(error => {
        expect(error).toBe('loadAllPosts:method => Error: Execution Error');
        done();
      });
    });
  });

  describe('loadAPost', () => {
    it('should retrieve data', (done) => {
      fetchMock.mockResponseOnce(JSON.stringify({ data: 'ok', status: 200 }));
      PostService.loadAPost(expect.any(Number)).then(data => {
        expect(data).toEqual({ data: 'ok', status: 200 });
        done();
      });
    });

    it('should reject request', (done) => {
      fetchMock.mockRejectOnce(Error('Rejected Resquest'));
      PostService.loadAPost(expect.any(Number)).then().catch(error => {
        expect(error).toBe('loadAPost:request => Error: Rejected Resquest');
        done();
      });
    });

    it('should reject request when fetch throw an error', (done) => {
      jest.spyOn(globalThis, 'fetch').mockImplementationOnce(() => {
        throw new Error('Execution Error');
      });
      PostService.loadAPost(expect.any(Number)).then().catch(error => {
        expect(error).toBe('loadAPost:method => Error: Execution Error');
        done();
      });
    });
  });

  describe('loadAPostComments', () => {
    it('should retrieve data', (done) => {
      fetchMock.mockResponseOnce(JSON.stringify({ data: 'ok', status: 200 }));
      PostService.loadAPostComments(expect.any(Number)).then(data => {
        expect(data).toEqual({ data: 'ok', status: 200 });
        done();
      });
    });

    it('should reject request', (done) => {
      fetchMock.mockRejectOnce(Error('Rejected Resquest'));
      PostService.loadAPostComments(expect.any(Number)).then().catch(error => {
        expect(error).toBe('loadAPostComments:request => Error: Rejected Resquest');
        done();
      });
    });

    it('should reject request when fetch throw an error', (done) => {
      jest.spyOn(globalThis, 'fetch').mockImplementationOnce(() => {
        throw new Error('Execution Error');
      });
      PostService.loadAPostComments(expect.any(Number)).then().catch(error => {
        expect(error).toBe('loadAPostComments:method => Error: Execution Error');
        done();
      });
    });
  });

  describe('addCommentAtPost', () => {
    it('should retrieve data', (done) => {
      fetchMock.mockResponseOnce(JSON.stringify({ data: 'ok', status: 200 }));
      PostService.addCommentAtPost(expect.any(Number), expect.anything()).then(data => {
        expect(data).toEqual({ data: 'ok', status: 200 });
        done();
      });
    });

    it('should reject request', (done) => {
      fetchMock.mockRejectOnce(Error('Rejected Resquest'));
      PostService.addCommentAtPost(expect.any(Number), expect.anything()).then().catch(error => {
        expect(error).toBe('addCommentAtPost:request => Error: Rejected Resquest');
        done();
      });
    });

    it('should reject request when fetch throw an error', (done) => {
      jest.spyOn(globalThis, 'fetch').mockImplementationOnce(() => {
        throw new Error('Execution Error');
      });
      PostService.addCommentAtPost(expect.any(Number), expect.anything()).then().catch(error => {
        expect(error).toBe('addCommentAtPost:method => Error: Execution Error');
        done();
      });
    });
  });
});