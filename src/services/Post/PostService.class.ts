import { IPost } from '../../views/components/PostList/IPost';
import { IComment } from '../../IScope';
import { ICommentForm } from '../../views/components/CommentForm/ICommentForm';

export default abstract class PostService {
  static readonly BASEURL: string = 'http://localhost:9001';

  static loadAllPosts(): Promise<IPost[]> {
    const ENDPOINT = '/posts';
    return new Promise((resolve, reject) => {
      try {
        fetch(this.BASEURL.concat(ENDPOINT), {
          method: 'GET'
        }).then(data => resolve(data.json()))
          .catch(error => reject(`loadAllPosts:request => ${error}`));
      } catch (e) {
        reject(`loadAllPosts:method => ${e}`);
      }
    });
  }

  static loadAPost(id: number): Promise<IPost> {
    const ENDPOINT = `/posts/${id}`;
    return new Promise((resolve, reject) => {
      try {
        fetch(this.BASEURL.concat(ENDPOINT), {
          method: 'GET'
        }).then(data => resolve(data.json()))
          .catch(error => reject(`loadAPost:request => ${error}`));
      } catch (e) {
        reject(`loadAPost:method => ${e}`);
      }
    });
  }

  static loadAPostComments(id: number): Promise<IComment[]> {
    const ENDPOINT = `/posts/${id}/comments`;
    return new Promise((resolve, reject) => {
      try {
        fetch(this.BASEURL.concat(ENDPOINT), {
          method: 'GET'
        }).then(data => resolve(data.json()))
          .catch(error => reject(`loadAPostComments:request => ${error}`));
      } catch (e) {
        reject(`loadAPostComments:method => ${e}`);
      }
    });
  }

  static addCommentAtPost(id: number, comment: ICommentForm): Promise<any> {
    const headers = new Headers();
    headers.append('Content-Type', 'application/json');

    const ENDPOINT = `/posts/${id}/comments`;
    return new Promise((resolve, reject) => {
      try {
        fetch(this.BASEURL.concat(ENDPOINT), {
          method: 'POST',
          headers: headers,
          body: JSON.stringify(comment),
        }).then(data => resolve(data.json()))
          .catch(error => reject(`addCommentAtPost:request => ${error}`));
      } catch (e) {
        reject(`addCommentAtPost:method => ${e}`);
      }
    });
  }
}