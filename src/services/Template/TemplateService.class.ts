export default abstract class TemplateService {
  static template: string;
  static scope: Object;
  static isList: boolean;

  public static init(_template: string, _scope: Object, _isList: boolean = false) {
    this.template = _template;
    this.scope = _scope;
    this.isList = _isList;
    return this;
  }

  public static processHTML(): string {
    const templateBuild = (scope: Object, template: string) => {
      return eval('`' + template + '`');
    };

    if (this.isList) {
      let templateProcessed: string;
      if ((this.scope as any[]).length > 0) {
        templateProcessed = Object.keys(this.scope).map(key => templateBuild.bind(null, (this.scope as any)[key], this.template).call()).join('');
      } else {
        templateProcessed = templateBuild.bind(null, this.scope, '<h5>Oops, we have nothing to show here.</h5><p>We are sorry for the disruption. Please, try it again. If the error persists check your internet connection.</p>').call();
      }
      return templateProcessed;
    }

    return templateBuild.bind(null, this.scope, this.template).call();
  }
}
