import TemplateService from './TemplateService.class';

describe('Template Service', () => {
  let template: string;

  it('should process single template', () => {
    template = '<div>${scope.name}<div>';
    TemplateService.init(template, { name: 'Testing Template'});

    const processed = TemplateService.processHTML();
    expect(processed).toBe('<div>Testing Template<div>');
  });

  it('should process a list template', () => {
    template = '<div>${scope.item}</div>';
    const scope = [
      { item: 'item 1' },
      { item: 'item 2' },
      { item: 'item 3' }
    ];
    TemplateService.init(template, scope, true);
    const processed = TemplateService.processHTML();
    expect(processed).toBe('<div>item 1</div><div>item 2</div><div>item 3</div>');
  });

  it('should process a list template and retrieve an error message when scope is empty', () => {
    template = '<div>${scope.item}</div>';
    const scope: Array<any> = [];
    TemplateService.init(template, scope, true);
    const processed = TemplateService.processHTML();
    expect(processed).toBe('<h5>Oops, we have nothing to show here.</h5><p>We are sorry for the disruption. Please, try it again. If the error persists check your internet connection.</p>');
  });
});