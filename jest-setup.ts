import { GlobalWithFetchMock } from 'jest-fetch-mock';
import 'regenerator-runtime/runtime.js';

const customGlobal: GlobalWithFetchMock = global as GlobalWithFetchMock;

customGlobal.fetch = require('jest-fetch-mock');
customGlobal.fetchMock = customGlobal.fetch;